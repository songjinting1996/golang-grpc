/*
 * @Author: your name
 * @Date: 2021-04-05 20:40:22
 * @LastEditTime: 2021-04-06 17:31:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/grpc/server/server.go
 */
package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"

	user "grpc/pro/user"
)

const (
	port = ":50051"
)

//UserService ...
type UserService struct {
	// 实现 User 服务的业务对象
}

//UserIndex ...
func (userService *UserService) UserIndex(ctx context.Context, in *user.UserIndexRequest) (*user.UserIndexResponse, error) {
	log.Printf("receive user index request: page %d page_size %d", in.Page, in.PageSize)

	return &user.UserIndexResponse{
		Err: 0,
		Msg: "success",
		Data: []*user.UserEntity{
			{Name: "songjinting", Age: 24},
			{Name: "liuyutong", Age: 25},
		},
	}, nil
}

//UserView ...
func (userService *UserService) UserView(ctx context.Context, in *user.UserViewRequest) (*user.UserViewResponse, error) {
	log.Printf("receive user view request: uid %d", in.Uid)

	return &user.UserViewResponse{
		Err:  0,
		Msg:  "success",
		Data: &user.UserEntity{Name: "chunchun", Age: 25},
	}, nil
}

//UserPost ...
func (userService *UserService) UserPost(ctx context.Context, in *user.UserPostRequest) (*user.UserPostResponse, error) {
	log.Printf("receive user post request: name %s password %s age %d", in.Name, in.Password, in.Age)

	return &user.UserPostResponse{
		Err: 0,
		Msg: "success",
	}, nil
}

//UserDelete ...
func (userService *UserService) UserDelete(ctx context.Context, in *user.UserDeleteRequest) (*user.UserDeleteResponse, error) {
	log.Printf("receive user delete request: uid %d", in.Uid)

	return &user.UserDeleteResponse{
		Err: 0,
		Msg: "success",
	}, nil
}

func main() {
	//监听TCP端口
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	//创建服务器端对象 grpcServer
	grpcServer := grpc.NewServer()

	//将 UserService(其包含需要被调用的服务端接口) 注册到服务器端对象 grpcServer 上（保证当接收到请求后,能够发现相应的服务器接口并进行相应的逻辑处理）
	user.RegisterUserServer(grpcServer, &UserService{})

	//注册反射服务（这个服务是CLI使用的跟服务本身没有关系）
	// reflection.Register(grpcServer)

	//服务器端对象 grpcServer 开始接收
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
