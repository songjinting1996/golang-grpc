/*
 * @Author: your name
 * @Date: 2021-04-05 20:42:14
 * @LastEditTime: 2021-04-06 17:29:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /songjintinglinux/go/src/grpc/client/client.go
 */

package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"google.golang.org/grpc"

	user "grpc/pro/user"
)

const (
	address = "localhost:50051"
)

func main() {
	//创建与服务器端的连接句柄
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	//创建客户端对象 userClient
	userClient := user.NewUserClient(conn)

	//设定请求超时时间 3s
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	//发送 UserIndex请求,等待同步响应,得到回调后响应结果
	userIndexReponse, err := userClient.UserIndex(ctx, &user.UserIndexRequest{Page: 1, PageSize: 12})
	if err != nil {
		log.Printf("user index could not greet: %v", err)
	}
	if 0 == userIndexReponse.Err {
		log.Printf("user index success: %s", userIndexReponse.Msg)
		// 包含 UserEntity 的数组列表
		userEntityList := userIndexReponse.Data
		for _, row := range userEntityList {
			fmt.Println(row.Name, row.Age)
		}
	} else {
		log.Printf("user index error: %d", userIndexReponse.Err)
	}

	//发送 UserView请求,等待同步响应,得到回调后响应结果
	userViewResponse, err := userClient.UserView(ctx, &user.UserViewRequest{Uid: 1})
	if err != nil {
		log.Printf("user view could not greet: %v", err)
	}
	if 0 == userViewResponse.Err {
		log.Printf("user view success: %s", userViewResponse.Msg)
		userEntity := userViewResponse.Data
		fmt.Println(userEntity.Name, userEntity.Age)
	} else {
		log.Printf("user view error: %d", userViewResponse.Err)
	}

	//发送 UserPost请求,等待同步响应,得到回调后响应结果
	userPostReponse, err := userClient.UserPost(ctx, &user.UserPostRequest{Name: "songjinting", Password: "123456", Age: 24})
	if err != nil {
		log.Printf("user post could not greet: %v", err)
	}
	if 0 == userPostReponse.Err {
		log.Printf("user post success: %s", userPostReponse.Msg)
	} else {
		log.Printf("user post error: %d", userPostReponse.Err)
	}

	//发送 UserDelete请求,等待同步响应,得到回调后响应结果
	userDeleteReponse, err := userClient.UserDelete(ctx, &user.UserDeleteRequest{Uid: 1})
	if err != nil {
		log.Printf("user delete could not greet: %v", err)
	}
	if 0 == userDeleteReponse.Err {
		log.Printf("user delete success: %s", userDeleteReponse.Msg)
	} else {
		log.Printf("user delete error: %d", userDeleteReponse.Err)
	}
}
